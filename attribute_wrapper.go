package form

import (
	"reflect"

	"golang.org/x/net/html"
)

// AttributeWrapper is a wrapper which extracts extra html attributes from a given struct tag.
// This is useful for overwriting certain html attributes via struct tags.
type AttributeWrapper struct {
	FieldTag string
}

func NewAttributeWrapper() AttributeWrapper {
	return AttributeWrapper{
		FieldTag: "attr",
	}
}

func (wrapper *AttributeWrapper) Encode(context *EncoderContext, typeOf reflect.Type, valueOf reflect.Value) error {
	if extraTag, ok := context.LookupTag(wrapper.FieldTag); ok {
		tags := ParseTag(extraTag)
		for key, value := range tags {
			context.Attributes = append(context.Attributes, html.Attribute{
				Key: key,
				Val: value,
			})
		}
	}
	return nil
}

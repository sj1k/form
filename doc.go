/*
Package form provides utilities for iterating over go types and generating HTML forms.

By default this package is designed to work with other packages;

# Decoding Form Values

The [gorilla/schema] package is used to decode a submitted form back into Go types.

Custom naming schemes can be passed so other decoders are supported.

# Rendering HTML

The [x/net/html] package is used for rendering the HTML form data and keeping track of
related information such as [html.Attribute] and the overall element tree.


[x/net/html]: https://pkg.go.dev/golang.org/x/net/html
[gorilla/schema]: https://github.com/gorilla/schema
*/
package form

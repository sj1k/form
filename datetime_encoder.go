package form

import (
	"reflect"
	"time"

	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
)

const DATETIME_FORMAT = "2006-01-02T15:04:05"

// EncodeDateTime encodes a [time.Time] value.
// This reads the "type" attribute from any StructTag information and switches time format based on requested type.
func EncodeDateTime(context *EncoderContext, typeOf reflect.Type, value reflect.Value) error {
	node := html.Node{
		Type:     html.ElementNode,
		Data:     atom.Input.String(),
		DataAtom: atom.Input,
		Attr:     context.Attributes,
	}

	NodeSetAttr(&node, atom.Name.String(), context.Name, NoOverwrite)

	timeFormat := DATETIME_FORMAT
	typeString := "datetime-local"

	if inputTag, ok := context.LookupTag(context.Encoder.FormTag); ok {
		attrs := ParseTag(inputTag)
		if timeAttr, ok := attrs["type"]; ok {
			switch timeAttr {
			case "time":
				timeFormat = time.TimeOnly
				typeString = "time"
			case "date":
				timeFormat = time.DateOnly
				typeString = "date"
			}
		}
	}

	NodeSetAttr(&node, atom.Type.String(), typeString, NoOverwrite)

	if _, hasValue := NodeHasAttr(&node, atom.Value.String()); !hasValue {
		if value.IsValid() && value.CanInterface() {
			v := value.Interface()
			if timeValue, ok := v.(time.Time); ok {
				NodeSetAttr(&node, atom.Value.String(), timeValue.Format(timeFormat), NoOverwrite)
			}
		}

	}

	context.ParentNode.AppendChild(&node)
	return nil
}

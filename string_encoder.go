package form

import (
	// "fmt"
	"reflect"

	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
)

func EncodeString(context *EncoderContext, typeOf reflect.Type, value reflect.Value) error {
	node := html.Node{
		Type:     html.ElementNode,
		Data:     atom.Input.String(),
		DataAtom: atom.Input,
		Attr:     context.Attributes,
	}
	NodeSetAttr(&node, atom.Name.String(), context.Name, NoOverwrite)
	NodeSetAttr(&node, atom.Type.String(), "text", NoOverwrite)

	if value.IsValid() && !value.IsZero() {
		NodeSetAttr(&node, atom.Value.String(), value.String(), NoOverwrite)
	}

	context.ParentNode.AppendChild(&node)
	return nil
}

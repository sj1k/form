package form

import (
	"reflect"
	"time"

	"golang.org/x/net/html"
)

type Encoder struct {
	SchemaTag string
	FormTag   string

	customTypes map[reflect.Type]CustomEncoder
	customKinds map[reflect.Kind]CustomEncoder
	wrappers    []CustomEncoder
}

// NewEmptyEncoder creates an empty encoder with no registered encoders.
// Useful for building a completely custom set of encoders.
//
// For more information on custom encoders see:
// [Encoder.RegisterTypeEncoder]
// [Encoder.RegisterKindEncoder]
func NewEmptyEncoder() Encoder {
	customTypes := make(map[reflect.Type]CustomEncoder)
	customKinds := make(map[reflect.Kind]CustomEncoder)
	encoder := Encoder{
		customTypes: customTypes,
		customKinds: customKinds,
		SchemaTag:   "schema",
		FormTag:     "form",
	}
	return encoder
}

// NewEncoder creates an encoder with the default registered encoders.
func NewEncoder() Encoder {
	encoder := NewEmptyEncoder()

	structEncoder := NewStructEncoder(DefaultStructNamer)
	arrayEncoder := NewArrayEncoder(DefaultArrayNamer)
	encoder.RegisterKindEncoder(reflect.Struct, &structEncoder)
	encoder.RegisterKindEncoder(reflect.Array, &arrayEncoder)
	encoder.RegisterKindEncoder(reflect.Slice, &arrayEncoder)

	encoder.RegisterTypeEncoderFunc("", EncodeString)
	encoder.RegisterTypeEncoderFunc(int(0), EncodeInt)
	encoder.RegisterTypeEncoderFunc(int8(0), EncodeInt)
	encoder.RegisterTypeEncoderFunc(int16(0), EncodeInt)
	encoder.RegisterTypeEncoderFunc(int32(0), EncodeInt)
	encoder.RegisterTypeEncoderFunc(int64(0), EncodeInt)
	encoder.RegisterTypeEncoderFunc(uint(0), EncodeUint)
	encoder.RegisterTypeEncoderFunc(uint8(0), EncodeUint)
	encoder.RegisterTypeEncoderFunc(uint16(0), EncodeUint)
	encoder.RegisterTypeEncoderFunc(uint32(0), EncodeUint)
	encoder.RegisterTypeEncoderFunc(uint64(0), EncodeUint)
	encoder.RegisterTypeEncoderFunc(float64(0.0), EncodeFloat64)
	encoder.RegisterTypeEncoderFunc(float32(0.0), EncodeFloat64)
	encoder.RegisterTypeEncoderFunc(true, EncodeBool)
	encoder.RegisterTypeEncoderFunc(time.Time{}, EncodeDateTime)
	return encoder
}

// RegisterTypeEncoder uses the [reflect.Type] of the given value to register a [CustomEncoder]
// To use a function for encoding see [ENcoder.RegisterTypeEncoderFunc]
func (encoder *Encoder) RegisterTypeEncoder(v interface{}, custom_encoder CustomEncoder) {
	typeOf := reflect.TypeOf(v)
	encoder.customTypes[typeOf] = custom_encoder
}

func (encoder *Encoder) RegisterTypeEncoderFunc(v interface{}, encoder_func CustomEncoderFunc) {
	encoder.RegisterTypeEncoder(v, encoder_func)
}

// RegisterKindEncoder uses the [reflect.Kind] of the given value to register a [CustomEncoder]
// To use a function for encoding see [Encoder.RegisterKindEncoderFunc]
func (encoder *Encoder) RegisterKindEncoder(kind reflect.Kind, custom_encoder CustomEncoder) {
	encoder.customKinds[kind] = custom_encoder
}

func (encoder *Encoder) RegisterKindEncoderFunc(kind reflect.Kind, encoder_func CustomEncoderFunc) {
	encoder.RegisterKindEncoder(kind, encoder_func)
}

// SetWrappers sets / overwrites which wrappers will be run before each encoder.
func (encoder *Encoder) SetWrappers(wrappers ...CustomEncoder) {
	encoder.wrappers = wrappers
}

// AppendWrappers appends more wrappers to be run before each encoder.
func (encoder *Encoder) AppendWrappers(wrappers ...CustomEncoder) {
	encoder.wrappers = append(encoder.wrappers, wrappers...)
}

// EncodeValue starts encoding the given value and appends results to the given parent.
// This is the main entry point for form generation.
func (encoder *Encoder) EncodeValue(parent *html.Node, v interface{}) error {
	typeOf := reflect.TypeOf(v)
	valueOf := reflect.ValueOf(v)
	name := ""
	context := EncoderContext{
		ParentNode: parent,
		Name:       name,
		Encoder:    encoder,
	}
	return encoder.Encode(&context, typeOf, valueOf)
}

func (encoder *Encoder) Encode(context *EncoderContext, typeOf reflect.Type, valueOf reflect.Value) error {
	kind := typeOf.Kind()
	if kind == reflect.Ptr {
		return encoder.Encode(context, typeOf.Elem(), valueOf.Elem())
	}

	chain := ChainEncoders(encoder.wrappers...)

	if valueOf.CanInterface() {
		v := valueOf.Interface()
		if customEncoder, ok := v.(CustomEncoder); ok {
			wrapper := WrapEncoder(chain, customEncoder)
			return wrapper.Encode(context, typeOf, valueOf)
		}
	}

	if customType, ok := encoder.customTypes[typeOf]; ok {
		wrapper := WrapEncoder(chain, customType)
		return wrapper.Encode(context, typeOf, valueOf)
	}

	valueKind := valueOf.Kind()
	if customKind, ok := encoder.customKinds[valueKind]; ok {
		wrapper := WrapEncoder(chain, customKind)
		return wrapper.Encode(context, typeOf, valueOf)
	}
	return UnknownError{
		Kind: kind,
		Type: typeOf,
	}
}

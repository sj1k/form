package form_test

import (
	"fmt"
	"os"

	"gitlab.com/sj1k/form"
	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
)

func ExampleNewEncoder_basic_example() {

	type Signup struct {
		Name string `label:"Some label here!"`
		Pass string `label:"Enter a password!" attr:"type=password"`
		Age  int
	}

	// AttrbuteWrapper extracts and applies attributes from struct tags.
	// This will load attributes from the `attr` tag.
	attrWrapper := form.NewAttributeWrapper()

	// LabelWrapper wraps the input with a <label> element if the `label:""` tag exists.
	labelWrapper := form.NewLabelWrapper()

	encoder := form.NewEncoder()
	encoder.SetWrappers(&labelWrapper, &attrWrapper)

	parent := CreateFormNode()

	err := encoder.EncodeValue(&parent, &Signup{})
	if err != nil {
		fmt.Println(err)
		return
	}

	PrettyRender(os.Stdout, &parent)

	// Output:
	// <form>
	//     <label>Some label here!
	//         <input name="Name" type="text"/>
	//     </label>
	//     <label>Enter a password!
	//         <input type="password" name="Pass"/>
	//     </label>
	//     <input name="Age" type="number" value="0" min="-9223372036854775808" max="9223372036854775807"/>
	// </form>
}

func CreateFormNode() html.Node {
	return html.Node{
		Type:     html.ElementNode,
		Data:     atom.Form.String(),
		DataAtom: atom.Form,
	}
}

package form

import (
	"reflect"

	"golang.org/x/net/html"
)

type EncoderContext struct {
	Encoder    *Encoder
	ParentNode *html.Node
	StructTag  *reflect.StructTag
	Name       string
	Attributes []html.Attribute
}

func (context *EncoderContext) LookupTag(name string) (string, bool) {
	var result string
	if context.StructTag == nil {
		return result, false
	}
	return context.StructTag.Lookup(name)
}

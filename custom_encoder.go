package form

import (
	"reflect"
)

// CustomEncoder is an interface for anything that can encode a reflected type and value.
type CustomEncoder interface {
	Encode(*EncoderContext, reflect.Type, reflect.Value) error
}

type CustomEncoderFunc func(*EncoderContext, reflect.Type, reflect.Value) error

func (encoderFunc CustomEncoderFunc) Encode(context *EncoderContext, typeOf reflect.Type, valueOf reflect.Value) error {
	return encoderFunc(context, typeOf, valueOf)
}

module gitlab.com/sj1k/form

go 1.20

require (
	github.com/Joker/hpp v1.0.0
	github.com/gorilla/schema v1.2.0
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.8.1
	gitlab.com/sj1k/form/label_parser v0.0.0-20221120002134-bbdf6cf2f1be
	gitlab.com/sj1k/form/schema_parser v0.0.0-20221120223740-1b2a8e84349a
	golang.org/x/net v0.2.0
)

require (
	github.com/Vektah/goparsify v0.0.0-20180611020250-3e20a3b9244a // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/vektah/goparsify v0.0.0-20180611020250-3e20a3b9244a // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

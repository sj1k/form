package form

import (
	"fmt"
	"reflect"
	"strings"

	"github.com/pkg/errors"
)

type StructNamingScheme func(total, name string) string

func DefaultStructNamer(total, name string) string {
	joiner := ""
	if total != "" {
		joiner = "."
	}
	return fmt.Sprintf("%s%s%s", total, joiner, name)
}

type StructEncoder struct {
	NamingScheme StructNamingScheme
}

func NewStructEncoder(namer StructNamingScheme) StructEncoder {
	return StructEncoder{
		NamingScheme: namer,
	}
}

func (structEncoder *StructEncoder) Encode(context *EncoderContext, typeOf reflect.Type, valueOf reflect.Value) error {
	fields := valueOf.NumField()
	for i := 0; i < fields; i++ {
		fieldValue := valueOf.Field(i)
		structField := typeOf.Field(i)
		fieldType := structField.Type
		childName := context.Name
		tagName := ""

		if formTag, ok := structField.Tag.Lookup(context.Encoder.SchemaTag); ok {
			split := strings.Split(formTag, ",")

			if len(split) > 0 {
				first := split[0]
				if first == "-" {
					continue
				}
				if first != "" {
					tagName = first
				}
			}

			if len(split) > 1 {
				second := split[1]
				if second == "omitempty" && fieldValue.IsZero() {
					continue
				}
			}
		}

		if tagName == "" {
			tagName = structField.Name
		}

		if len(tagName) > 0 {
			childName = structEncoder.NamingScheme(context.Name, tagName)
		}
		childContext := EncoderContext{
			Name:       childName,
			ParentNode: context.ParentNode,
			Encoder:    context.Encoder,
			StructTag:  &structField.Tag,
		}
		err := context.Encoder.Encode(&childContext, fieldType, fieldValue)
		if err != nil {
			return errors.Wrap(err, "error encoding struct field")
		}
	}
	return nil
}

package form

import (
	"fmt"
	"reflect"

	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
)

func EncodeUint(context *EncoderContext, typeOf reflect.Type, value reflect.Value) error {
	node := html.Node{
		Type:     html.ElementNode,
		Data:     atom.Input.String(),
		DataAtom: atom.Input,
		Attr:     context.Attributes,
	}
	NodeSetAttr(&node, atom.Name.String(), context.Name, NoOverwrite)
	NodeSetAttr(&node, atom.Type.String(), "number", NoOverwrite)

	if value.IsValid() && value.CanUint() {
		NodeSetAttr(&node, atom.Value.String(), fmt.Sprintf("%d", value.Uint()), NoOverwrite)
	}

	SetValidRange(&node, value)

	context.ParentNode.AppendChild(&node)
	return nil
}

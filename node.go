package form

import (
	"golang.org/x/net/html"
)

const (
	Overwrite SetMode = iota
	NoOverwrite
)

// SetMode is used to know if [NodeSetAttr] should overwrite attributes or not.
// See [NodeSetAttr]
type SetMode int

func NodeHasAttr(node *html.Node, key string) (int, bool) {
	var foundIndex int
	for index, attr := range node.Attr {
		if attr.Key == key {
			return index, true
		}
	}
	return foundIndex, false
}

func NodeSetAttr(node *html.Node, key string, value string, setmode SetMode) {
	attr := html.Attribute{
		Key: key,
		Val: value,
	}

	index, ok := NodeHasAttr(node, attr.Key)

	if ok && setmode != Overwrite {
		return
	}

	if ok {
		node.Attr[index] = attr
	} else {
		node.Attr = append(node.Attr, attr)
	}
}

func NodeGetAttr(node *html.Node, key string) (html.Attribute, bool) {
	var attr html.Attribute
	index, hasAttr := NodeHasAttr(node, key)
	if hasAttr {
		return node.Attr[index], true
	}
	return attr, hasAttr
}

func NodeDeleteAttr(node *html.Node, key string) {
	index, ok := NodeHasAttr(node, key)
	if ok {
		node.Attr = append(node.Attr[:index], node.Attr[index+1:]...)
	}
}

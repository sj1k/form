package form_test

import (
	"fmt"
	"os"
	"reflect"
	"time"

	"gitlab.com/sj1k/form"
	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
)

func ExampleNewEncoder() {
	type MyForm struct {
		A  string
		A1 *string

		B  int
		B1 *int

		C  float64
		C1 *float64

		D  time.Time
		D1 *time.Time

		E  uint
		E1 *uint

		F  bool
		F1 *bool
	}

	encoder := form.NewEncoder()

	parent := html.Node{
		Type:     html.ElementNode,
		Data:     atom.Form.String(),
		DataAtom: atom.Form,
	}

	a1 := "some string"
	b1 := 2121
	c1 := 3333.0
	d1 := time.Date(2024, 8, 28, 0, 0, 0, 0, time.UTC)
	e1 := uint(302910)
	f1 := true

	err := encoder.EncodeValue(&parent, MyForm{
		A:  "A string",
		A1: &a1,
		B:  2,
		B1: &b1,
		C:  3.0,
		C1: &c1,
		D:  time.Date(2023, 8, 28, 0, 0, 0, 0, time.UTC),
		D1: &d1,
		E:  uint(109812309),
		E1: &e1,
		F:  false,
		F1: &f1,
	})
	if err != nil {
		fmt.Println(err)
		return
	}

	PrettyRender(os.Stdout, &parent)

	// Output:
	// <form>
	//     <input name="A" type="text" value="A string"/>
	//     <input name="A1" type="text" value="some string"/>
	//     <input name="B" type="number" value="2" min="-9223372036854775808" max="9223372036854775807"/>
	//     <input name="B1" type="number" value="2121" min="-9223372036854775808" max="9223372036854775807"/>
	//     <input name="C" type="number" step="any" value="3.000000"/>
	//     <input name="C1" type="number" step="any" value="3333.000000"/>
	//     <input name="D" type="datetime-local" value="2023-08-28T00:00:00"/>
	//     <input name="D1" type="datetime-local" value="2024-08-28T00:00:00"/>
	//     <input name="E" type="number" value="109812309" min="0" max="18446744073709551615"/>
	//     <input name="E1" type="number" value="302910" min="0" max="18446744073709551615"/>
	//     <input name="F" type="checkbox"/>
	//     <input name="F1" type="checkbox" checked="true"/>
	// </form>

}

type ParagraphEncoder struct{}

func (paragraph *ParagraphEncoder) Encode(context *form.EncoderContext, typeOf reflect.Type, valueOf reflect.Value) error {
	context.ParentNode.AppendChild(&html.Node{
		Type: html.TextNode,
		Data: fmt.Sprintf("%v", valueOf),
	})
	return nil
}

func ExampleEncoder_RegisterTypeEncoder() {
	encoder := form.NewEmptyEncoder()
	encoder.RegisterTypeEncoder("string", &ParagraphEncoder{})

	parent := html.Node{
		Type:     html.ElementNode,
		Data:     atom.P.String(),
		DataAtom: atom.P,
	}
	err := encoder.EncodeValue(&parent, "Some Value")
	if err != nil {
		fmt.Println(err)
		return
	}
	PrettyRender(os.Stdout, &parent)

	// Output:
	// <p>Some Value</p>
}

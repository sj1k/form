package form

import (
	"reflect"

	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
)

// FieldsetWrapper is a wrapper which creates <fieldset> wrappers around nodes when a given struct tag is found.
// Optionally creates a <legend> if the struct tag has content.
type FieldsetWrapper struct {
	FieldTag string
}

func NewFieldsetWrapper() FieldsetWrapper {
	return FieldsetWrapper{
		FieldTag: "fieldset",
	}
}

func (wrapper *FieldsetWrapper) Encode(context *EncoderContext, typeOf reflect.Type, valueOf reflect.Value) error {
	if fieldTag, ok := context.LookupTag(wrapper.FieldTag); ok {
		node := html.Node{
			Type:     html.ElementNode,
			Data:     atom.Fieldset.String(),
			DataAtom: atom.Fieldset,
			Attr:     context.Attributes,
		}

		// Only append the legend if there is any content
		if len(fieldTag) != 0 {
			legend := html.Node{
				Type:     html.ElementNode,
				Data:     atom.Legend.String(),
				DataAtom: atom.Legend,
			}

			text := html.Node{
				Type: html.TextNode,
				Data: fieldTag,
			}

			legend.AppendChild(&text)
			node.AppendChild(&legend)
		}

		context.ParentNode.AppendChild(&node)
		context.ParentNode = &node
	}
	return nil
}

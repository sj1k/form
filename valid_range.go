package form

import (
	"fmt"
	"reflect"

	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
)

const (
	// Plain `int` and `uint` types are different sizes on 32 and 64 bit systems
	// The following calculates the min / max ranges.
	// https://stackoverflow.com/questions/6878590/the-maximum-value-for-an-int-type-in-go
	MAX_UINT = ^uint(0)
	MAX_INT  = int(MAX_UINT >> 1)
	MIN_INT  = -MAX_INT - 1

	MIN_INT64 = "-9223372036854775808"
	MAX_INT64 = "9223372036854775807"

	MIN_INT32 = "-2147483648"
	MAX_INT32 = "2147483647"

	MIN_INT16 = "-32768"
	MAX_INT16 = "32767"

	MIN_INT8 = "-128"
	MAX_INT8 = "127"

	MAX_UINT64 = "18446744073709551615"
	MAX_UINT32 = "4294967295"
	MAX_UINT16 = "65535"
	MAX_UINT8  = "255"
)

var (
	RangeLookup = map[reflect.Kind]Range{
		reflect.Int: {
			Min: fmt.Sprintf("%d", MIN_INT),
			Max: fmt.Sprintf("%d", MAX_INT),
		},
		reflect.Int64: {
			Min: MIN_INT64,
			Max: MAX_INT64,
		},
		reflect.Int32: {
			Min: MIN_INT32,
			Max: MAX_INT32,
		},
		reflect.Int16: {
			Min: MIN_INT16,
			Max: MAX_INT16,
		},
		reflect.Uint: {
			Min: "0",
			Max: fmt.Sprintf("%d", MAX_UINT),
		},
		reflect.Int8: {
			Min: MIN_INT8,
			Max: MAX_INT8,
		},
		reflect.Uint64: {
			Min: "0",
			Max: MAX_UINT64,
		},
		reflect.Uint32: {
			Min: "0",
			Max: MAX_UINT32,
		},
		reflect.Uint16: {
			Min: "0",
			Max: MAX_UINT16,
		},
		reflect.Uint8: {
			Min: "0",
			Max: MAX_UINT8,
		},
	}
)

type Range struct {
	Min string
	Max string
}

// SetValidRange will attempt to lookup and set min / max values if they do not already exist as an attrbute on the node.
func SetValidRange(node *html.Node, value reflect.Value) {
	kind := value.Kind()
	if validRange, ok := RangeLookup[kind]; ok {
		NodeSetAttr(node, atom.Min.String(), validRange.Min, NoOverwrite)
		NodeSetAttr(node, atom.Max.String(), validRange.Max, NoOverwrite)
	}
}

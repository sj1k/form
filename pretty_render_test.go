package form_test

import (
	"bytes"
	"fmt"
	"io"

	"github.com/Joker/hpp"
	"golang.org/x/net/html"
)

// PrettyRender renders the input node as html and then prettifies it with [hpp.Print]
// This should generally only be used for testing or display purposes.
func PrettyRender(writer io.Writer, node *html.Node) error {
	var out bytes.Buffer
	err := html.Render(&out, node)
	if err != nil {
		return err
	}
	output := hpp.Print(&out)
	fmt.Fprintln(writer, string(output))
	return nil
}

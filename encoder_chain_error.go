package form

import (
	"fmt"
)

type EncoderChainError struct {
	Err   error
	Index int
}

func (chainError EncoderChainError) Error() string {
	return fmt.Sprintf("error running encoder #%d; %s", chainError.Index, chainError.Err.Error())
}

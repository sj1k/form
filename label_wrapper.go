package form

import (
	// "fmt"
	"reflect"

	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
)

type LabelWrapper struct {
	FieldTag string
}

func NewLabelWrapper() LabelWrapper {
	return LabelWrapper{
		FieldTag: "label",
	}
}

func (wrapper *LabelWrapper) Encode(context *EncoderContext, typeOf reflect.Type, valueOf reflect.Value) error {
	if labelTag, ok := context.LookupTag(wrapper.FieldTag); ok {
		if len(labelTag) > 0 {
			label := html.Node{
				Type:     html.ElementNode,
				Data:     atom.Label.String(),
				DataAtom: atom.Label,
			}

			text := html.Node{
				Type: html.TextNode,
				Data: labelTag,
			}

			label.AppendChild(&text)

			context.ParentNode.AppendChild(&label)
			context.ParentNode = &label
		}
	}
	return nil
}

package form_test

import (
	"fmt"
	"os"
	"time"

	"gitlab.com/sj1k/form"
	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
)

func ExampleEncodeDateTime() {
	type MyForm struct {
		Date     time.Time `form:"type=date"`
		Time     time.Time `form:"type=time"`
		DateTime time.Time
	}
	encoder := form.NewEncoder()
	parent := html.Node{
		Type:     html.ElementNode,
		Data:     atom.Form.String(),
		DataAtom: atom.Form,
	}
	err := encoder.EncodeValue(&parent, MyForm{
		Date:     time.Date(2023, 9, 4, 0, 0, 0, 0, time.UTC),
		Time:     time.Date(2023, 9, 4, 0, 0, 0, 0, time.UTC),
		DateTime: time.Date(2023, 9, 4, 0, 0, 0, 0, time.UTC),
	})
	if err != nil {
		fmt.Println(err)
		return
	}

	PrettyRender(os.Stdout, &parent)

	// Output:
	// <form>
	//     <input name="Date" type="date" value="2023-09-04"/>
	//     <input name="Time" type="time" value="00:00:00"/>
	//     <input name="DateTime" type="datetime-local" value="2023-09-04T00:00:00"/>
	// </form>

}

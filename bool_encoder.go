package form

import (
	// "fmt"
	"reflect"

	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
)

func EncodeBool(context *EncoderContext, typeOf reflect.Type, value reflect.Value) error {
	node := html.Node{
		Type:     html.ElementNode,
		Data:     atom.Input.String(),
		DataAtom: atom.Input,
		Attr:     context.Attributes,
	}
	NodeSetAttr(&node, atom.Name.String(), context.Name, NoOverwrite)
	NodeSetAttr(&node, atom.Type.String(), "checkbox", NoOverwrite)

	if value.IsValid() {
		checked := value.Bool()
		if checked {
			NodeSetAttr(&node, atom.Checked.String(), "true", NoOverwrite)
		}
	}
	context.ParentNode.AppendChild(&node)
	return nil
}

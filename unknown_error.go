package form

import (
	"fmt"
	"reflect"
)

type UnknownError struct {
	Kind reflect.Kind
	Type reflect.Type
}

func (err UnknownError) Error() string {
	return fmt.Sprintf("unable to find a matching encoder: type=%s, kind=%s", err.Type.String(), err.Kind.String())
}

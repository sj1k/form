package form

import (
	"fmt"
	"reflect"

	"github.com/pkg/errors"
)

type ArrayNamingScheme func(total string, index int) string

func DefaultArrayNamer(total string, index int) string {
	if total != "" {
		return fmt.Sprintf("%s.%d", total, index)
	}
	return fmt.Sprintf("%d", index)
}

type ArrayEncoder struct {
	NamingScheme ArrayNamingScheme
}

func NewArrayEncoder(namer ArrayNamingScheme) ArrayEncoder {
	return ArrayEncoder{
		NamingScheme: namer,
	}
}

func (arrayEncoder *ArrayEncoder) Encode(context *EncoderContext, typeOf reflect.Type, valueOf reflect.Value) error {
	length := valueOf.Len()
	name := context.Name
	for i := 0; i < length; i++ {
		itemValue := valueOf.Index(i)
		itemType := itemValue.Type()

		childName := arrayEncoder.NamingScheme(name, i)
		childContext := EncoderContext{
			Name:       childName,
			ParentNode: context.ParentNode,
			Encoder:    context.Encoder,
		}
		err := context.Encoder.Encode(&childContext, itemType, itemValue)
		if err != nil {
			return errors.Wrap(err, "error encoding array item")
		}
	}
	return nil
}

package form_test

import (
	"fmt"
	"os"

	"gitlab.com/sj1k/form"
	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
)

func ExampleFieldsetWrapper() {
	type MyForm struct {
		With    string `fieldset:"With a label"`
		Without string `fieldset:""`
	}

	fieldset := form.NewFieldsetWrapper()
	// attributes := form.NewAttributeWrapper()

	encoder := form.NewEncoder()
	encoder.SetWrappers(
		// &attributes,
		&fieldset,
	)

	parent := html.Node{
		Type:     html.ElementNode,
		Data:     atom.Form.String(),
		DataAtom: atom.Form,
	}

	err := encoder.EncodeValue(&parent, MyForm{
		With:    "with label",
		Without: "without label",
	})

	if err != nil {
		fmt.Println(err)
		return
	}

	PrettyRender(os.Stdout, &parent)

	// Output:
	// <form>
	//     <fieldset>
	//         <legend>With a label</legend>
	//         <input name="With" type="text" value="with label"/>
	//     </fieldset>
	//     <fieldset>
	//         <input name="Without" type="text" value="without label"/>
	//     </fieldset>
	// </form>
}

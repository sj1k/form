package form

import (
	"reflect"
)

// EncoderChain calls a chain of other [CustomEncoder]'s.
// The encoders are called in regular iteration order.
type EncoderChain struct {
	Encoders []CustomEncoder
}

func ChainEncoders(encoders ...CustomEncoder) EncoderChain {
	return EncoderChain{
		Encoders: encoders,
	}
}

func (chain EncoderChain) Encode(context *EncoderContext, typeOf reflect.Type, valueOf reflect.Value) error {
	for index, encoder := range chain.Encoders {
		err := encoder.Encode(context, typeOf, valueOf)
		if err != nil {
			return EncoderChainError{
				Err:   err,
				Index: index,
			}
		}
	}
	return nil
}

package form_test

import (
	"testing"

	"gitlab.com/sj1k/form"
	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
)

func TestUnknownError(t *testing.T) {
	type CustomType struct{}

	encoder := form.NewEmptyEncoder()

	parent := html.Node{
		Type:     html.ElementNode,
		Data:     atom.Form.String(),
		DataAtom: atom.Form,
	}

	err := encoder.EncodeValue(&parent, CustomType{})
	if err == nil {
		t.Error("Attempting to encode an unknown type should produce an error")
		t.Fail()
		return
	}

	if len(err.Error()) == 0 {
		t.Error("Error message has no value; UnknownError may have no error message.")
		t.Fail()
		return
	}
}

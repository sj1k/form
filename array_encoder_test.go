package form_test

import (
	// "reflect"
	"os"

	"gitlab.com/sj1k/form"
	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
)

func ExampleArrayEncoder() {
	data := []string{
		"Name 1",
		"Name 2",
		"Name 3",
	}

	encoder := form.NewEncoder()
	parent := html.Node{
		Type:     html.ElementNode,
		Data:     atom.Form.String(),
		DataAtom: atom.Form,
	}

	encoder.EncodeValue(&parent, data)

	PrettyRender(os.Stdout, &parent)

	// Output:
	// <form>
	//     <input name="0" type="text" value="Name 1"/>
	//     <input name="1" type="text" value="Name 2"/>
	//     <input name="2" type="text" value="Name 3"/>
	// </form>
}

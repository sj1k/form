package form_test

import (
	"fmt"
	"os"

	"gitlab.com/sj1k/form"
	"golang.org/x/net/html"
	"golang.org/x/net/html/atom"
)

func ExampleAttributesetWrapper() {
	type MyForm struct {
		Type  string `attr:"type=password"`
		Value string `attr:"value=overwritten"`
	}

	attributes := form.NewAttributeWrapper()

	encoder := form.NewEncoder()
	encoder.SetWrappers(
		&attributes,
	)

	parent := html.Node{
		Type:     html.ElementNode,
		Data:     atom.Form.String(),
		DataAtom: atom.Form,
	}

	err := encoder.EncodeValue(&parent, MyForm{
		Type:  "type attr",
		Value: "should be overwritten",
	})

	if err != nil {
		fmt.Println(err)
		return
	}

	PrettyRender(os.Stdout, &parent)

	// Output:
	// <form>
	//     <input type="password" name="Type" value="type attr"/>
	//     <input value="overwritten" name="Value" type="text"/>
	// </form>
}

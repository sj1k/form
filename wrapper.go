package form

import (
	"reflect"

	"github.com/pkg/errors"
)

type EncoderWrapper struct {
	Wrapper CustomEncoder
	Encoder CustomEncoder
}

// WrapEncoder creates a new EncoderWrapper with a given wrapper and encoder.
// If you need multiple wrappers it may be worth using an [EncoderChain]
func WrapEncoder(wrapper CustomEncoder, encoder CustomEncoder) EncoderWrapper {
	return EncoderWrapper{
		Wrapper: wrapper,
		Encoder: encoder,
	}
}

func (wrapper EncoderWrapper) Encode(context *EncoderContext, typeOf reflect.Type, valueOf reflect.Value) error {
	wrappedErr := wrapper.Wrapper.Encode(context, typeOf, valueOf)
	if wrappedErr != nil {
		return errors.Wrap(wrappedErr, "error running wrapper")
	}
	return wrapper.Encoder.Encode(context, typeOf, valueOf)
}

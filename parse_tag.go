package form

import (
	"strings"
)

func ParseTag(tag string) map[string]string {
	result := make(map[string]string)
	for _, section := range strings.Split(tag, ",") {
		if strings.Contains(section, "=") {
			split := strings.Split(section, "=")
			key := split[0]
			value := split[1]
			result[key] = value
		}
	}
	return result
}
